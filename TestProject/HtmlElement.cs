using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace TestProject
{
    public class HtmlElement 
    {
        
        
        public string Name { get; private set; }
        public string  Text { get; private set; }
        
        protected List<HtmlElement> Children;

        protected HtmlElement()
        {
            Children = new List<HtmlElement>();
        }

        protected HtmlElement(string name, string text)
        {
            Name = name;
            Text = text;
            Children = new List<HtmlElement>();
        }

        public void AddChildren(string name, string text)
        {
            Children.Add(new HtmlElement(name, text));
        }
        
        public override string ToString()
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append($"<{Name}>");
            foreach (var child in Children)
            {
                strBuilder.Append($"<{child.Name}>{child.Text}</{child.Name}>");
            }

            strBuilder.Append($"</{Name}>");
            return strBuilder.ToString();
        }
        
        public static HtmlElementBuilder Create(string name)=> new HtmlElementBuilder(new HtmlElement(name,""));
        
        
      
    }
    public class HtmlElementBuilder
    {
        protected  HtmlElement root;

        public HtmlElementBuilder(HtmlElement element)
        {
            root = element;
        }

        

        public HtmlElementBuilder AddChild(string name, string text)
        {
            root.AddChildren(name, text);
            return this;
        }

        public override string ToString()
        {
            return root.ToString();
        }

        public static implicit operator HtmlElement(HtmlElementBuilder builder)
        {
            return builder.root;
        }
    }
    

}
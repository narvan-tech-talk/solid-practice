namespace TestProject
{
    public class Person
    {
        public string Name { get; set; }
        public string Family { get; set; }
    }

    public class Machine
    {
        public string Model { get; set; }
        public int ValveNo { get; set; }
    }
}
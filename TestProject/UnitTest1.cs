using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mime;
using System.Windows.Input;
using NUnit.Framework;
using TestProject.Calculator;
using ICommand = TestProject.Calculator.ICommand;

namespace TestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            /*Expression<Func<int, int,int>> add = (int a, int b) => a + b;
            
            var t = add.Compile();
            t(3, 4);*/
        }

        [Test]
        public void Test1()
        {
             HtmlElement element = HtmlElement.Create("ul").AddChild("ol", "navid").AddChild("ol", "behrad"); 
             Console.WriteLine(element);
             Assert.Pass(); 
        }
        
        
        [Test]
        public void Test2()
        {
            var p = new Person();
            var m = new Machine();

           // var o = p + m;
        }


        [Test]
        public void Test4()
        {
            // I think we need a command builder for configurating the command list
            var executer = new CommandExecutor();
            ICommand c1 = new PlusCommand(new Operand[]{new Operand(3), new Operand(4)  });
            
            ICommand c2 = new PlusCommand(new Operand[]{new Operand(4), new Operand(5) , (Operand)c1.Result });
            executer.commands.Add(c1);
            executer.commands.Add(c2);

            executer.Execute();

        }
    }
   


  
}
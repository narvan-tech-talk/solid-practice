using System;
using System.Collections.Generic;

namespace TestProject.Calculator
{
    public class CommandExecutor
    {
        public List<ICommand>  commands  = new List<ICommand>();

        public void Execute()
        {
            AbstractOperand t = new Operand(0);
            foreach (var command in commands)
            {
                t = command.Execute();
            }
            //Console.WriteLine(t);
        }
    }
}
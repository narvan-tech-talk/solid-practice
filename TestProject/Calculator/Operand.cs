namespace TestProject.Calculator
{
    public class Operand : AbstractOperand
    {
        public override string ToString()
        {
            return Value.ToString();
        }

        public Operand(double val)
        {
            Value = val;
        }
    }
}
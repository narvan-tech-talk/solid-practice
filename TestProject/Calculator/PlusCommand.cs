using System.Net;

namespace TestProject.Calculator
{
    public class PlusCommand : ICommand
    {
        public Operand[] Operads { get; private set; }

        public PlusCommand(Operand[] opr)
        {
            Operads = opr;
            Result =  new Operand(0);
        }

        public AbstractOperand Result { get; set; } 

        public AbstractOperand Execute()
        {
                
            AbstractOperand answer = new Operand(0);
            foreach (var opr in Operads)
            {
                answer = new Operand(opr.Value + answer.Value);
            }
            Result.Value = answer.Value;
            return  answer ;
        }
    }
}
namespace TestProject.Calculator
{
    public interface ICommand
    {
        AbstractOperand Result { get; set; }
       AbstractOperand Execute();
    }
}
namespace TestProject.Calculator
{
    public abstract class AbstractOperand
    {
        public double Value { get;   set; }
    }
}